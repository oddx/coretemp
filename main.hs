import System.Process (createProcess, proc, std_out, StdStream(CreatePipe))
import GHC.IO.Handle (hGetContents)
import Data.List (isInfixOf, dropWhileEnd,  genericLength) 

--reduce each core temp to average and print
main = do
   let temperature        = isInfixOf "temperature"
   (_, Just handle, _, _) <- createProcess (proc "sysctl" ["dev.cpu."]) {std_out = CreatePipe}
   contents               <- hGetContents handle
   putStrLn (show . round . average . map toInt . map parse . filter temperature . lines $ contents)

parse xs =
   let (_, set) = break (== ' ') xs
   in tail . init . dropPrecision  $ set

dropPrecision xs =
   dropWhileEnd (/= '.') xs

average xs =
   realToFrac (sum xs) / genericLength xs

toInt x =
   read x :: Int

